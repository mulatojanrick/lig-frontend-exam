# L I G Frontend Exam

# FRONT END

## Project setup
```
cd fe
```

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


# API

## Project setup
```
cd api
```

```
npm install
```

```
npm run start
```