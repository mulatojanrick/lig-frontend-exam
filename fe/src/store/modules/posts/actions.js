import _ from 'lodash';
import api from '../../../plugins/axios';

export default {
  async getPost(context, params) {
    try {
      context.commit('SET_LOADING', true);
      let id = _.get(params, 'id', 0)
      let response = await api.client.post('', {
        query: `
        {
          post(id: ${id}) {
            content,
            title,
            id,
            createdAt,
            image,
            comments {
              id,
              postId,
              content,
              createdAt
            }
          }
        }
        `
      });

      if (_.get(response, 'errors', null)) {
        throw _.get(response, 'errors.[0].message')
      }

      let post = _.get(response, 'data.post', {});
      context.commit('SET_ITEM', post)
      return post;
    } catch(err) {
      console.log(err);
      context.commit('SET_ITEM', {});
      throw err;
    } finally {
      context.commit('SET_LOADING', false);
    }

  },

  async fetchPosts(context, params) {
    try {
      context.commit('SET_LOADING', true);
      let limit = _.get(params, 'limit', 6);
      let offset = _.get(params, 'offset', 0);
      let response = await api.client.post('', {
        query: `
        {
          posts(pagination:{limit: ${limit}, offset: ${offset}}) {
            content,
            title,
            id,
            createdAt,
            image
          }
        }
        `
      });

      if (_.get(response, 'errors', null)) {
        throw _.get(response, 'errors.[0].message')
      }

      let data = _.get(response, 'data.posts', [])
      let posts = context.state.items;
      if (data.length > 0) {
        posts = [
          ...context.state.items,
          ...data
        ];
      } else {
        context.commit('SET_ALL_ITEMS_LOADED', true);
      }

      context.commit('SET_ITEMS', posts)
      return posts;
    } catch(err) {
      context.commit('SET_ITEMS', []);
      console.log(err);
      throw err;
    } finally {
      context.commit('SET_LOADING', false);
    }
  },
  async addPost(context, params) {
    try {
      context.commit('SET_LOADING', true)
      let response = await api.client.post('', {
        query: `
          mutation AddPost($post: PostInput!) {
            addPost(post: $post) {
              content,
              title,
              id,
              createdAt,
              image
            }
          }
        `,
        variables: {
          post: params,
        }
      });

      if (_.get(response, 'errors', null)) {
        throw _.get(response, 'errors.[0].message')
      }

      let post = _.get(response, 'data.addPost', {})
      context.commit('SET_ITEM', post)
      return post;
    } catch(err) {
      console.log(err, 'error');
      throw err
    } finally {
      context.commit('SET_LOADING', false)
    }
  },

  async updatePost(context, params) {
    try {
      context.commit('SET_LOADING', true)
      let response = await api.client.post('', {
        query: `
          mutation UpdatePost($post: PostInput!) {
            updatePost(post: $post) {
              content,
              title,
              id,
              createdAt,
              image
            }
          }
        `,
        variables: {
          post: params,
        }
      });

      if (_.get(response, 'errors', null)) {
        throw _.get(response, 'errors.[0].message')
      }

      let post = _.get(response, 'data.updatePost', {})
      context.commit('SET_ITEM', post)
      return post;
    } catch(err) {
      console.log(err, 'error');
      throw err
    } finally {
      context.commit('SET_LOADING', false)
    }
  },
}
