import _ from 'lodash';

export default {
  SET_ITEMS(state, data) {
    state.items = data;
    if (data.length === 0) {
      state.allItemsLoaded = false;
    }
  },
  SET_ITEM(state, data) {
    state.item = data;
  },
  SET_LOADING(state, data) {
    state.loading = data;
  },
  SET_ALL_ITEMS_LOADED(state, data) {
    console.log(data, 'set all items')
    state.allItemsLoaded = data;
  },
  ADD_COMMENT(state, data) {
    if (_.get(state, 'item.comments')) {
      state.item.comments.unshift(data);
    }
  }
}
