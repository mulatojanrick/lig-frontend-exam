import _ from 'lodash';
import api from '../../../plugins/axios';

export default {
  async authenticate(context, params) {
    try {
      context.commit('SET_LOADING', true)
      let response = await api.client.post('', {
        query: `
          mutation Authenticate($email: String!, $password: String!) {
            authenticate(email: $email, password: $password)
          }
        `,
        variables: {
          email: _.get(params, 'email', ''),
          password: _.get(params, 'password', '')
        }
      });

      if (!_.get(response, 'data.authenticate', '')) {
        throw 'Email or Password is incorrect';
      }

      if (_.get(response, 'errors', null)) {
        throw _.get(response, 'errors.[0].message')
      }
      // @TODO MUTATE RESPONSE IF RETURN A TOKEN
      context.commit('AUTHENTICATE_USER', {
        isAuthenticated: true,
        token: response.data.authenticate
      })
      return response;
    } catch(err) {
      console.log(err);
      throw err;
    } finally {
      context.commit('SET_LOADING', false)
    }

  },

  async register(context, params) {
    try {
      context.commit('SET_LOADING', true)
      let response = await api.client.post('', {
        query: `
          mutation Register($email: String!, $password: String!) {
            register(email: $email, password: $password)
          }
        `,
        variables: {
          email: _.get(params, 'email'),
          password: _.get(params, 'password')
        }
      });

      if (_.get(response, 'errors', null)) {
        throw _.get(response, 'errors.[0].message')
      }

      return response;
    } catch(err) {
      console.log(err, 'error');
      throw err
    } finally {
      context.commit('SET_LOADING', false)
    }
  },

  logout(context, params) {
    context.commit('AUTHENTICATE_USER', {
      isAuthenticated: false,
      token: ''
    });
    localStorage.clear();
  }
}
