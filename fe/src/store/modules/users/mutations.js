import _ from 'lodash';
export default {
  AUTHENTICATE_USER(state, data) {
    console.log(data, 'data to mutate');
    state.isAuthenticated = _.get(data, 'isAuthenticated', false);
    state.token = _.get(data, 'token', '')
  },
  SET_LOADING(state, data) {
    state.loading = data;
  }
}
