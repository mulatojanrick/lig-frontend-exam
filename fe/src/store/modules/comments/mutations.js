export default {
  SET_LOADING(state, data) {
    state.loading = data;
  },
  SET_ITEMS(state, data) {
    state.items = data;
  },
  ADD_COMMENT(state, data) {
    state.items.unshift(data);
  }
}
