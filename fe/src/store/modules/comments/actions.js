import _ from 'lodash';
import api from '../../../plugins/axios';

export default {
  async addComment(context, params) {
    try {
      context.commit('SET_LOADING', true)
      let response = await api.client.post('', {
        query: `
          mutation AddComment($postId: Int!, $content: String!) {
            addComment(postId: $postId, content: $content) {
              id,
              postId,
              content,
              createdAt
            }
          }
        `,
        variables: {
          postId: parseInt(_.get(params, 'postId')),
          content: _.get(params, 'comment')
        }
      });

      if (_.get(response, 'errors', null)) {
        throw _.get(response, 'errors.[0].message')
      }

      let comment = _.get(response, 'data.addComment', {})
      context.commit('ADD_COMMENT', comment)
      return response;
    } catch(err) {
      console.log(err, 'error');
      throw err
    } finally {
      context.commit('SET_LOADING', false)
    }
  },
  async fetchComments(context, params) {
    try {
      context.commit('SET_LOADING', true);
      let id = _.get(params, 'id', 0)
      let response = await api.client.post('', {
        query: `
        {
          post(id: ${id}) {
            comments {
              id,
              postId,
              content,
              createdAt
            }
          }
        }
        `
      });

      if (_.get(response, 'errors', null)) {
        throw _.get(response, 'errors.[0].message')
      }

      context.commit('SET_ITEMS', _.get(response, 'data.post.comments', []))
      return response;
    } catch(err) {
      console.log(err);
      context.commit('SET_ITEMS', []);
      throw err;
    } finally {
      context.commit('SET_LOADING', false);
    }

  },
}
