import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Post from '../views/Post.vue'
import PostManage from '../components/posts/PostManage.vue'
import NorFound from '../components/core/NotFound.vue'
import store from '../store/index';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      topbarAction: {
        title: 'Login',
        to: {name: 'Login'}
      }
    }
  },
  {
    path: '*',
    redirect: '/404',
  },
  {
    path: '/404',
    meta: {
    },
    component: NorFound
    // component: () => import('./components/base/NotFound.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      topbarAction: {
        title: 'Close',
        to: {name: 'Home'}
      }
    },
    beforeEnter: (to, from, next) => {
      let token = localStorage.getItem('authtoken');
      if (!store.state.users.isAuthenticated || !store.state.users.token || !token) {
        next();
      } else {
        next({
          name: 'Home'
        })
      }
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      topbarAction: {
        title: 'Close',
        to: {name: 'Home'}
      }
    },
    beforeEnter: (to, from, next) => {
      let token = localStorage.getItem('authtoken');
      if (!store.state.users.isAuthenticated || !store.state.users.token || !token) {
        next();
      } else {
        next({
          name: 'Home'
        })
      }
    }
  },
  {
    path: '/post',
    name: 'PostIndex',
    component: () => import('../components/EmptyRouter'),
    children: [
      {
        path: ':id/view',
        name: 'Post',
        component: Post,
        meta: {
          topbarAction: {
            title: 'Login',
            to: {name: 'Login'}
          }
        }
      },
      {
        path: ':id/edit',
        name: 'PostEdit',
        component: PostManage,
        meta: {
          requiresAuth: true,
          topbarAction: {
            title: 'Login',
            to: {name: 'Login'},
          }
        }
      },
      {
        path: 'new',
        name: 'PostNew',
        component: PostManage,
        meta: {
          requiresAuth: true,
          topbarAction: {
            title: 'Login',
            to: {name: 'Login'}
          }
        }
      },
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.users.isAuthenticated || !store.state.users.token) {
      localStorage.clear();
      sessionStorage.clear();
      next({
        name: 'Home',
      });
    } else {
      next();
    }
  } else {
    next();
  }
})
export default router
