import Vue from 'vue'
import vuelidate from 'vuelidate';
import _ from 'lodash';
import moment from 'moment';

import App from './App.vue'
import router from './router'
import store from './store'
import VueButton from '@/components/core/VueButton.vue'
import VueTextField from '@/components/core/VueTextField.vue'
import VueModal from '@/components/core/VueModal.vue'
import VueTextarea from  '@/components/core/VueTextarea.vue'
import ErrorBoundary from '@/components/core/ErrorBoundary.vue'

Vue.config.productionTip = false
Vue.use(vuelidate);

Vue.component('vue-button', VueButton);
Vue.component('vue-text-field', VueTextField);
Vue.component('vue-modal', VueModal);
Vue.component('vue-textarea', VueTextarea);
Vue.component('error-boundary', ErrorBoundary);

Vue.filter('humanizeDate', function (value ) {
  if (typeof value !== 'string') return ''
  var now = moment(new Date()); //todays date
  var end = moment(value); // another date
  var duration = moment.duration(end.diff(now));
  return moment.duration(duration).humanize(true)
});

Vue.filter('date', function (value, format = 'YYYY.MM.DD') {
  if (typeof value !== 'string') return ''
  return moment(value).format(format)
});
Object.defineProperty(Vue.prototype, '$_', { value: _ });

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

